package fr.umlv.conc;

import java.util.Objects;

/**
 * Note: this code does several stupid things !
 */
public class RendezVous<V> {

	private V value;
	private final Object lock = new Object();

	/*
	public void set(V value) {
		Objects.requireNonNull(value);
		synchronized (lock) {
			this.value = value;
		}
	}

	public V get() throws InterruptedException {

		while (true) {
			// Thread.sleep(1); // then comment this line !
			synchronized (lock) {
				if (value != null) {
					return value;
				}
			}
		}
	}
	*/
	
	public void set(V value) {
		Objects.requireNonNull(value);
		synchronized (lock) {
			this.value = value;
			lock.notify();
		}
	}

	public V get() throws InterruptedException {

		synchronized (lock) {
			while (value == null) {
				lock.wait();
			}
			return value;
		}
	}

	public static void main(String[] args) throws InterruptedException {
		RendezVous<String> rendezVous = new RendezVous<>();
		new Thread(() -> {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				throw new AssertionError(e);
			}
			rendezVous.set("hello");
		}).start();

		System.out.println(rendezVous.get());
	}

	/*
	 * ---------- Exercice 1 ----------
	 * 
	 * 4. Une attente active est une attente durant laquelle une thread va vérifier périodiquement si une condition est remplie.
	 *    Dans la méthode get la condition value != null est vérifiée à chaque tour de boucle.
	 *    
	 * 5. 
	 */
}