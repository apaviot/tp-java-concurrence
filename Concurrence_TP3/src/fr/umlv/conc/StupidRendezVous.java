package fr.umlv.conc;

import java.util.Objects;

/**
 * Note: this code does several stupid things !
 */
public class StupidRendezVous<V> {
  private V value;
  
  public void set(V value) {
    Objects.requireNonNull(value);
    this.value = value;
  }
  
  public V get() throws InterruptedException {
    while(value == null) {
        //Thread.sleep(1);  // then comment this line !
    }
    return value;
  }
  
  public static void main(String[] args) throws InterruptedException {
    StupidRendezVous<String> rendezVous = new StupidRendezVous<>();
    new Thread(() -> {
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        throw new AssertionError(e);
      }
      rendezVous.set("hello");
    }).start();
    
    System.out.println(rendezVous.get());
  }
  
  /* ---------- Exercice 1 ----------
   * 
   * 1. Lorsqu'on exécute le code ce dernier affiche "hello" après plusieurs secondes.
   *
   * 2. En commentant l'instruction Thread.sleep(1), le code s'exécute sans fin.
   *    Le JIT peut stocker la valeur de value == null dans une variable temporaire.
   *    Le sleep va empecher que le JIT ait le temps d'optimiser le code.
   *    
   * 3. 
   */
}