package fr.umlv.conc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Exchanger<V> {

	private final ReentrantLock lock = new ReentrantLock();
	private final Condition affectedValues = lock.newCondition();
	private Object stockedValue = null;
	private ExchangerState state;

	public Exchanger() {
		lock.lock();
		try {
			state = ExchangerState.CREATED;
		} finally {
			lock.unlock();
		}
	}

	@SuppressWarnings("unchecked")
	public V exchange(V value) throws InterruptedException {
		lock.lock();
		try {
			if (state == ExchangerState.CREATED) {
				stockedValue = value;
				state = ExchangerState.USED;
				while (state != ExchangerState.ENDED) {
					affectedValues.await();
				}
				return (V) stockedValue;
			}
			affectedValues.signal();
			state = ExchangerState.ENDED;
			V tmp = (V) stockedValue;
			stockedValue = value;
			return tmp;
		} finally {
			lock.unlock();
		}
	}

	private enum ExchangerState {
		CREATED, USED, ENDED
	}
	
	/* ---------- Exercice 2 ----------
	 * 
	 * 1. Pour distinguer le premier et le second appel à la méthode exchange on peut utiliser
	 *    une énumération qui représente l'état de l'Exchanger.
	 * 
	 * 5. L'Exchanger n'est pas utilisable plus d'une fois car son état n'est pas réutilisable.
	 * 
	 */
}
