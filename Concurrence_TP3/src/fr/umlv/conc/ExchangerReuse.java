package fr.umlv.conc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ExchangerReuse<V> {

	private final ReentrantLock lock = new ReentrantLock();
	private final Condition isFull = lock.newCondition();
	private final Condition isWritable = lock.newCondition();
	private V stockedValue;
	private ExchangerState state;

	public ExchangerReuse() {
		lock.lock();
		try {
			state = ExchangerState.EMPTY;
		} finally {
			lock.unlock();
		}
	}

	public V exchange(V value) throws InterruptedException {
		lock.lock();
		try {
			while (true) {
				switch (state) {
				case EMPTY:
					stockedValue = value;
					state = ExchangerState.HALF_FULL;
					isWritable.signal(); // 1 place de libre
					while (state != ExchangerState.FULL) {
						isFull.await();
					}
					state = ExchangerState.EMPTY;
					isWritable.signal(); // 2 places de libre
					return stockedValue;
				case HALF_FULL:
					V tmp = stockedValue;
					stockedValue = value;
					isFull.signal();
					state = ExchangerState.FULL;
					return tmp;
				case FULL:
					while (state == ExchangerState.FULL) {
						isWritable.await(); // en attente d'une place
					}
				}
			}
		} finally {
			lock.unlock();
		}
	}

	private enum ExchangerState {
		EMPTY, HALF_FULL, FULL
	}
}
