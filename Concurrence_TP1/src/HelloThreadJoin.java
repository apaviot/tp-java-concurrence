import java.util.stream.IntStream;

public class HelloThreadJoin {

	private static Thread createThread(int threadNumber) throws InterruptedException {
		return new Thread(() -> {
			IntStream.range(0, 10).forEach(i -> System.out.println("hello " + threadNumber + " " + i));
		});
	}
	
	private static void startThreads(int count) throws InterruptedException {
		Thread threads[] = new Thread[count];
		for(int i = 0; i < count; i++) {
			Thread t  = createThread(i);
			t.start();
			threads[i] = t;
		}
		for(int i = 0; i < count; i++) {
			threads[i].join();
			System.out.println("La thread " + i + " a fini son Runnable");
		}
		System.out.println("Toutes les threads sont terminées");
	}

	public static void main(String[] args) throws InterruptedException {
		startThreads(4);
	}
}
