import java.util.stream.IntStream;

public class HelloThread {
	
	private static void startThreads(int count) throws InterruptedException {
		IntStream.range(0, count).forEach(threadNumber ->
			new Thread(() -> IntStream.range(0, 5000).forEach(i -> System.out.println("hello " + threadNumber + " " + i))).start()
		);
	}

	public static void main(String[] args) throws InterruptedException {		
		startThreads(4);
	}
	
	/* ----- Exercice 1 -----
	 * 
	 * 1. Un runnable permet de décrire un code à éxécuter.
	 * 
	 * 3. En exécutant le programme on remarque que les numéros affichés ne sont pas dans l'ordre (les numéros affichés ne vont
	 * 	  pas de 0 a 5000 successivement mais ils sont mélangés). Par ailleurs l'ordre d'affichage diffère à chaque exécution.
	 *    
	 *    Ceci est normal car l'ordonnanceur donne successivement la main aux différentes threads. Tant qu'un thread a la main,
	 *    elle exécute son code (affichage des nombres de 0 à 5000). Lorsqu'elle perd la main, son état est sauvegardé pour
	 *    pouvoir reprendre au même état lorsqu'elle reprend la main.
	 */

}
