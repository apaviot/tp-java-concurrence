import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class HelloListBug {

	private static void startThreads(int count) throws InterruptedException {
		MyArrayList<Integer> list = new MyArrayList<>();
		Thread threads[] = new Thread[count];
		Runnable runnable = () -> IntStream.range(0, 5000).forEach(list::add);
		
		for (int i = 0; i < count; i++) {
			Thread t = new Thread(runnable);
			t.start();
			threads[i] = t;
		}
		for (Thread t : threads) {
			t.join();
		}
			
		//System.out.println(list);
		System.out.println("Size : " + list.size());
		System.out.println(list);
	}

	public static void main(String[] args) throws InterruptedException {
		startThreads(4);
	}
	
	/* ----- Exercice 3 -----
	 * 
	 * 2. Pour ajouter une valeur dans le tableau, on récupère dans un premier temps l'indice auquel ajouter la valeur
	 *    puis on insert la valeur à cet indice pour enfin incrémenté celui-ci. Si une thread récupère l'indice en question
	 *    puis que l'ordonnanceur donne la main à une autre thread, alors cette dernière va récupérer le même indice. Les 
	 *    deux threads vont donc insérer un élément dans la même case du tableau. Par conséquent la première affectation
	 *    second écrasée par la seconde. Pour deux appels à la méthode "add" la taille de la liste n'aura donc augmentée que
	 *    de une unité.
	 *    Par ailleurs, si la seconde thread insert plusieurs éléments avant que la première ne récupère la main, alors
	 *    la taille de la liste sera faussée (cf exemple ci dessous)
	 *    
	 *    --> La thread 1 récupère l'indice auquel insérer l'élément.                    indice(T1) : 1
	 *    --> La thread 2 prend la main et récupère le même indice                       indice(T2) : 1
	 *    --> La thread 2 insert l'élément puis incrémente l'incide                      indice(T2) : 2
	 *    --> La thread 2 insert deux autres éléments                                    indice(T2) : 4
	 *    --> La thread 1 reprend la main, insert l'élément puis incrémente l'indice     indice(T1) : 2
	 *    
	 *    =====> L'incide courant de la liste indique que la taille de la liste est de 2 unités. En réalité, celle-ci
	 *           comporte 4 éléments.
	 *    
	 * 3. En ne fixant pas la capacité initiale de la liste, on observe que des ArrayOutOfBoundsException peuvent survenir
	 *    à l'exécution. Cela est dû au fait que le JIT est capable de réorganiser le code afin de l'optimiser. Dans notre cas,
	 *    le "size = s + 1" de la méthode "add" dans ArrayList peut être déplacer après l'appel à "grow(size + 1)" mais avant le
	 *    "Arrays.copyOf(...)". Par conséquent, si une autre thread prend la main avant que le "copyOf" ne soit éxécuté, "size"
	 *    aura bien été incrémenté mais le tableau n'aura pas été agrandit. En essayant d'affecter une valeur à l'indice "size"
	 *    une ArrayOutOfBoundsException sera donc levée.
	 *    
	 * 5. Les bonnes pratiques sur l'utilisation des locks à respecter sont :
	 * 	      - L'objet utilisé comme moniteur doit être privé et final
	 *             --> privé pour éviter qu'il ne soit utilisé comme moniteur dans du code extérieur
	 *             --> final car le fonctionnement de synchronized est basé les adresses des objets
	 *        - L'objet utilisé ne doit être ni une constante ni un wrapper (Integer, Double, ...)
	 *        - L'ensemble des sections critiques reliées doivent être concentrées au même endroit (en encapsulant une liste
	 *          dans la classe MyArrayList, on s'assure que tous les appels "add" sur cette liste seront synchronisés)
	 *          
	 */
	
	private static class MyArrayList<E> {
		private final List<E> list = new ArrayList<E>();
		
		public boolean add(E element) {
			synchronized(list) {
				return list.add(element);
			}
		}
		
		public int size() {
			synchronized(list) {
				return list.size();
			}
		}
		
		public String toString() {
			synchronized(list) {
				return list.toString();
			}
		}
	}
}
