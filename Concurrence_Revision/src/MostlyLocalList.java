import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.ArrayList;
import java.util.Arrays;

public class MostlyLocalList<E> {
	
	private static final VarHandle SIZE_REF;
	private static final VarHandle CAPACITY_REF;
	
	static {
		try {
			SIZE_REF = MethodHandles.lookup().findVarHandle(MostlyLocalList.class, "size", int.class);
			CAPACITY_REF = MethodHandles.lookup().findVarHandle(MostlyLocalList.class, "capacity", int.class);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new AssertionError(e);
		}
	}

	private Object[] elements;
	private final int threadLocalAllocatorSize;
	private ThreadLocal<ArrayList<E>> localElements;
	private volatile int size;
	private volatile int capacity;

	/**
	 * Create a MostlyLocalList asking to have at most {@code threadLocalAllocatorSize} elements in each thread specific
	 * list.
	 */
	public MostlyLocalList(int threadLocalAllocatorSize) {
		elements = new Object[threadLocalAllocatorSize];
		this.threadLocalAllocatorSize = threadLocalAllocatorSize;
		localElements = new ThreadLocal<>();
		capacity = threadLocalAllocatorSize;
	}

	/**
	 * Add an element to the a thread specific list and if the thread specific list contains
	 * {@code threadLocalAllocatorSize} elements, the elements are copied into the global dynamic array and the thread
	 * speficic list is cleared.
	 * 
	 * @param element
	 *            the element to add.
	 */
	public void add(E element) {
		if (localElements.get() == null) {
			ArrayList<E> elements = new ArrayList<>();
			elements.add(element);
			localElements.set(elements);
		} else {
			localElements.get().add(element);
			if (localElements.get().size() >= threadLocalAllocatorSize) {
				flush();
			}
		}
	}

	/**
	 * Copied all elements from the thread specific list into the global dynamic array and free all bookeeping data
	 * structured specific to the current thread.
	 */
	public void flush() {
		// Agrandissement du tableau
		if (localElements.get().size() + size >= capacity) {
			int newCapacity = capacity * 2;
			while (true) {
				int currentCapacity = capacity;
				if (CAPACITY_REF.compareAndSet(this, currentCapacity, newCapacity)) {
					elements = Arrays.copyOf(elements, newCapacity);
				}
			}
		}
		
		for (E el : localElements.get()) {
			while (true) {
				int currentSize = size;
				if (SIZE_REF.compareAndSet(this, currentSize, size + 1)) {
					elements[size-1] = el;
					break;
				}
			}
		}
	}

	public int size() {
		return size;
	}
}
