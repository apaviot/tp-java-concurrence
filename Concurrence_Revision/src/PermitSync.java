import java.util.function.Supplier;

public class PermitSync<V> {
	private final Object lock = new Object();
	private int currentThread = 0;
	private final int permits;

	public PermitSync(int permits) {
		this.permits = permits;
	}

	public V safe(Supplier<? extends V> supplier) throws InterruptedException{
		synchronized (lock) {
			while (currentThread >= permits) {
				lock.wait();
			}
			currentThread++;
		}
		try {
			return supplier.get();
		} finally {
			synchronized (lock) {
				currentThread--;
				lock.notifyAll();
			}
		}
	}
}