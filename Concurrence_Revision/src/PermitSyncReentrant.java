import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

public class PermitSyncReentrant<V> {
	private final ReentrantLock lock = new ReentrantLock();
	private final Condition isFull = lock.newCondition();
	private int currentThread = 0;
	private final int permits;

	public PermitSyncReentrant(int permits) {
		this.permits = permits;
	}

	public V safe(Supplier<? extends V> supplier) throws InterruptedException{
		lock.lock();
		try {
			while (currentThread >= permits) {
				isFull.await();
			}
			currentThread++;
		} finally {
			lock.unlock();
		}
		try {
			return supplier.get();
		} finally {
			lock.lock();
			try {
				currentThread--;
				isFull.signalAll();
			} finally {
				lock.unlock();
			}
		}
	}
}