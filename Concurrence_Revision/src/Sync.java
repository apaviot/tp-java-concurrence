import java.util.function.Supplier;

public class Sync<V> {
	private boolean isInSafe;
	private final Object lock = new Object();
	
	public boolean inSafe() {
		synchronized(lock) {
			return isInSafe;
		}
	}
	
	public V safe(Supplier<? extends V> supplier) throws InterruptedException {
		synchronized(lock) {
			while(isInSafe) {
				lock.wait();
			}
			isInSafe = true;
		}
		try {
			return supplier.get();
		} finally {
			synchronized(lock) {
				isInSafe = false;
				lock.notifyAll();
			}
		}
	}
}
