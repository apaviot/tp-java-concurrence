import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Counter {
	private int counter;
	private final Sync<Integer> sync = new Sync<>();
	
	public int count() throws InterruptedException {
		return sync.safe(() -> counter++);
	}
	
	public static void main(String[] args) {
		int nbThread = 2;
		ExecutorService executor = Executors.newFixedThreadPool(nbThread);
		Counter counter = new Counter();
		for(int i = 0; i < nbThread; i++) {
			executor.submit(new Thread(() -> {
				try {
					while (true) {
						System.out.println(counter.count());
					}
				} catch(InterruptedException e) {
					return;
				}
			}));
		}
	}
}
