package fr.umlv.conc;

import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;

public class RandomNumberGenerator {

	private AtomicLong x = new AtomicLong();

	public RandomNumberGenerator(long seed) {
		if (seed == 0) {
			throw new IllegalArgumentException("seed == 0");
		}
		x.set(seed);
	}

	/*
	 * public long next() { // Marsaglia's XorShift while (true) { long x = this.x.get(); long newValue = x; newValue ^=
	 * newValue >>> 12; newValue ^= newValue << 25; newValue ^= newValue >>> 27; if
	 * (this.x.compareAndSet(x, newValue)) { return newValue * 2685821657736338717L; } } }
	 */

	public long next() {
		return x.updateAndGet(l -> {
			l ^= l >>> 12;
			l ^= l << 25;
			l ^= l >>> 27;
			return l;
		}) * 2685821657736338717L;
	}

	public static void main(String[] args) throws InterruptedException {
		HashSet<Long> set1 = new HashSet<>();
		HashSet<Long> set2 = new HashSet<>();
		RandomNumberGenerator rng = new RandomNumberGenerator(1);
		Thread t = new Thread(() -> {
			for (int i = 0; i < 5_000; i++) {
				set1.add(rng.next());
			}
		});
		t.start();
		for (int i = 0; i < 5_000; i++) {
			set2.add(rng.next());
		}
		t.join();

		System.out.println("set1: " + set1.size() + ", set2: " + set2.size());
		set1.addAll(set2);
		System.out.println("union: " + set1.size());
		// if the RandomNumberGenerator is thread-safe the two sets should be disjoint
		// and the size of the union should be 10_000 the sum of the two sizes
	}

}