package fr.umlv.conc;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LockFreeStringList {

	static final class Entry {

		final String element;
		volatile Entry next;

		Entry(String element) {
			this.element = element;
		}
	}
	
	private final static VarHandle ENTRY_REF;
	private final static VarHandle TAIL_REF;
	
	static {
		try {
			ENTRY_REF = MethodHandles.lookup().findVarHandle(Entry.class, "next", Entry.class);
			TAIL_REF = MethodHandles.lookup().findVarHandle(LockFreeStringList.class, "tail", Entry.class);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new AssertionError(e.getCause());
		}
	}

	private final Entry head;
	private volatile Entry tail;

	public LockFreeStringList() {
		head = new Entry(null); // fake first entry
		tail = head;
	}

	/*
	public void addLast(String element) {
		Entry entry = new Entry(element);
		while (true) {
			Entry currentLast = getLastEntry();
			if (ENTRY_REF.compareAndSet(currentLast, null, entry)) {
				return;
			}
		}
	}
	*/
	public void addLast(String element) {
		Entry newLast = new Entry(element);
		while (true) {
			Entry currentLast = getLastEntry(tail);
			if (ENTRY_REF.compareAndSet(currentLast, null, newLast)) {
				boolean useless = TAIL_REF.compareAndSet(this, currentLast, newLast);
				return;
			}
		}
	}
	
	/*
	private Entry getLastEntry() {
		Entry last = head;
		for (;;) {
			Entry next = last.next;
			if (next == null) {
				return last;
			}
			last = next;
		}
	}
	*/
	
	private Entry getLastEntry(Entry start) {
		Entry last = start;
		for (;;) {
			Entry next = last.next;
			if (next == null) {
				return last;
			}
			last = next;
		}
	}

	public int size() {
		while (true) {
			Entry currentLast = getLastEntry(head);
			int count = 0;
			for (Entry e = head.next; e != null; e = e.next) {
				count++;
			}
			if (ENTRY_REF.compareAndSet(currentLast, null, null)) {
				return count;
			}
		}
	}

	private Runnable runnable(int id) {
		return () -> {
			for (int j = 0; j < 1_000; j++) {
				addLast(id + " " + j);
			}
		};
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		int threadCount = 5;
		LockFreeStringList list = new LockFreeStringList();
		List<Callable<Object>> tasks = IntStream.range(0, threadCount).mapToObj(list::runnable).map(Executors::callable)
			.collect(Collectors.toList());
		ExecutorService executor = Executors.newFixedThreadPool(threadCount);
		List<Future<Object>> futures = executor.invokeAll(tasks);
		executor.shutdown();
		for (Future<Object> f : futures) {
			f.get();
		}
		System.out.println(list.size());
	}
}