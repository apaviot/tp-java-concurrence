package fr.umlv.conc;

public class Bogus {

	private boolean stop;
	private final Object lock = new Object();

	public void runCounter() {
		int localCounter = 0;
		for (;;) {
			synchronized (lock) {
				if (stop) {
					break;
				}
				localCounter++;
			}
		}
		System.out.println(localCounter);
	}

	public void stop() {
		synchronized(lock) {
			stop = true;
		}
	}

	public static void main(String[] args) throws InterruptedException {
		Bogus bogus = new Bogus();
		Thread thread = new Thread(bogus::runCounter);
		thread.start();
		Thread.sleep(100);
		bogus.stop();
		thread.join();
	}
}