package fr.umlv.conc;

import java.util.ArrayDeque;

public class SynchronizedBlockingBuffer<E> {
	private final ArrayDeque<E> queue;
	private final int capacity;
	
	public SynchronizedBlockingBuffer(int capacity) {
		this.capacity = checkCapacity(capacity);
		queue = new ArrayDeque<>(capacity);
	}
	
	private static int checkCapacity(int capacity) {
		if (!(capacity > 0)) {
			throw new IllegalArgumentException("The capacity should be 1 or higher");
		}
		return capacity;
	}
	
	public void put(E element) throws InterruptedException {
		synchronized(queue) {
			while(queue.size() == capacity) {
				queue.wait();
			}
			queue.notifyAll(); // réveille les consommateurs
			queue.addFirst(element);
		}
	}
	
	public E take() throws InterruptedException {
		synchronized(queue) {
			while(queue.isEmpty()) {
				queue.wait();
			}
			queue.notifyAll(); // réveille les producteurs
			return queue.removeLast();
		}
	}
}
