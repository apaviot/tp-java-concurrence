package fr.umlv.conc;

import java.util.ArrayDeque;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class LockedBlockingBuffer<E> {

	private final ArrayDeque<E> queue;
	private final int capacity;
	private final ReentrantLock lock = new ReentrantLock();
	private final Condition isFull = lock.newCondition();
	private final Condition isEmpty = lock.newCondition();

	public LockedBlockingBuffer(int capacity) {
		this.capacity = checkCapacity(capacity);
		queue = new ArrayDeque<>(capacity);
	}

	private static int checkCapacity(int capacity) {
		if (!(capacity > 0)) {
			throw new IllegalArgumentException("The capacity should be 1 or higher");
		}
		return capacity;
	}

	public void put(E element) throws InterruptedException {
		lock.lock();
		try {
			while (queue.size() == capacity) {
				isFull.await();
			}
			isEmpty.signalAll();
			queue.addFirst(element);
		} finally {
			lock.unlock();
		}

	}

	public E take() throws InterruptedException {
		lock.lock();
		try {
			while (queue.isEmpty()) {
				isEmpty.wait();
			}
			isFull.signalAll();
			return queue.removeLast();
		} finally {
			lock.unlock();
		}
	}
}
