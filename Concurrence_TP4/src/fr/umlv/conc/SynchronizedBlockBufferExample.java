package fr.umlv.conc;

public class SynchronizedBlockBufferExample {
	
	private static Runnable createProducer(int threadNumber, int waitMillis, SynchronizedBlockingBuffer<String> queue) {
		return () -> {
			while (true) {		
				try {
					queue.put("hello " + threadNumber);
					Thread.sleep(waitMillis);
				} catch (InterruptedException e) {
					return;
				}
			}
		};
	}
	
	private static Runnable createConsummer(int waitMillis, SynchronizedBlockingBuffer<String> queue) {
		return () -> {
			while (true) {		
				try {
					System.out.println(queue.take());
					Thread.sleep(waitMillis);
				} catch (InterruptedException e) {
					return;
				}
			}
		};
	}

	public static void main(String[] args) {
		int capacity = 16;
		int[] producersWaitingTimes = {1, 4};
		int[] consummersWaitingTimes = {2, 3, 5};
		Thread[] producers = new Thread[producersWaitingTimes.length];
		Thread[] consummers = new Thread[consummersWaitingTimes.length];
		SynchronizedBlockingBuffer<String> queue = new SynchronizedBlockingBuffer<>(capacity);
		
		for(int i = 0; i < producersWaitingTimes.length; i++) {
			producers[i] = new Thread(createProducer(i, producersWaitingTimes[i], queue));
			producers[i].start();
		}
		
		for(int i = 0; i < consummersWaitingTimes.length; i++) {
			consummers[i] = new Thread(createConsummer(consummersWaitingTimes[i], queue));
			consummers[i].start();
		}
	}
}