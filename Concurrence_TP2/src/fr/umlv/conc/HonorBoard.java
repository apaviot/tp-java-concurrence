package fr.umlv.conc;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class HonorBoard {
	private String firstName;
	private String lastName;
	private final Lock lock = new ReentrantLock();

	/* ---------- Exercice 1 ----------
	 * 
	 * 1. Une classe n'est pas thread safe si son utilisation peut engendrer la création ou la visualisation d'un état
	 *    incohérent.
	 *    
	 * 2. La classe HonorBoard n'est pas thread safe pour plusieurs raisons :
	 *        ° lors d'un appel à la méthode set, une thread modifie la valeur de firstname mais perd la main avant de
	 *          modifier lastname. Une autre thread prend la main et modifie la valeur de firstname et de lastname. La
	 *          première thread reprend la main puis modifie la valeur de lastname. La valeur de firstname ne correspond
	 *          plus à la valeur de lastname.
	 *          
	 *        ° lors d'un appel à la méthode set, une thread modifie la valeur de firstname mais perd la main avant de
	 *          modifier lastname. Une autre thread prend la main et appelle la méthode toString. Le nom et le 
	 *          prénom affichés ne correspondent pas.
	 *    
	 *        ° Les valeurs lues par une thread peuvent être récupérées depuis un cache et non depuis la RAM. La valeur
	 *          récupérée n'est donc pas assurément la valeur réelle attendue.
	 *    
	 * 4. Il n'est pas possible de remplacer la ligne "System.out.println(board)" par 
	 *    "System.out.println(board.getFirstName() + ' ' + board.getLastName())" car si la thread qui tente de faire un
	 *    affichage perd la main après avoir récupéré firstName, une autre thread peut appeler la méthode set et modifier
	 *    firstName (ce qui ne sera pas pris en compte dans l'affichage).
	 * 
	 */
	
	/*
	public void set(String firstName, String lastName) {
		synchronized (lock) {
			this.firstName = firstName;
			this.lastName = lastName;
		}
	}
	
	@Override
	public String toString() {
		synchronized (lock) {
			return firstName + ' ' + lastName;
		}
	}
	*/
	
	
	/*---------- Exercice 2 ----------
	 * 
	 * 1. Ré-entrant signifie qu'une même thread peut acquérir plusieurs fois le même verrou.
	 * 
	 */
	public void set(String firstName, String lastName) {
		try {
			lock.lock();
			this.firstName = firstName;
			this.lastName = lastName;
		} finally {
			lock.unlock();
		}
	}
	
	@Override
	public String toString() {
		try {
			lock.lock();
			return firstName + ' ' + lastName;
		} finally {
			lock.unlock();
		}
	}

	public static void main(String[] args) {
		HonorBoard board = new HonorBoard();
		new Thread(() -> {
			for (;;) {
				board.set("John", "Doe");
			}
		}).start();

		new Thread(() -> {
			for (;;) {
				board.set("Jane", "Odd");
			}
		}).start();

		new Thread(() -> {
			for (;;) {
				System.out.println(board);
			}
		}).start();
	}
}