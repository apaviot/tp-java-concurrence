package fr.umlv.conc;

import java.util.Scanner;
import java.util.stream.IntStream;

public class CoitusInterruptusThread {

	private static final int THREAD_NUMBER = 4;
	
	/* ---------- Exercice 3 ----------
	 * 
	 * 1. Il n'est pas possible d'arrêter une thread de façon non coopérative car si celle-ci
	 *    possède un verrou, alors l'arrêter brutalement empêcherait le verrou d'être relaché.
	 *    
	 * 2. Une opération est bloquante si elle peut engendrer la mise en attente de threads. C'est le cas par
	 *    exemple de la méthode Thread.sleep().
	 * 
	 *    En cas d'opération bloquante, la méthode d'instance interrupt() lève une InterruptedException. Dans le
	 *    case d'une opération non bloquante, la méthode place un flag a true (indiquant qu'il y a une demande
	 *    d'interruption pour la thread courrante).
	 *    
	 *    Pour interrompre une thread en train d'effectuer une opération bloquante il faut placer l'appel à l'opération
	 *    bloquante dans un try { ... } catch(InterruptedException e) { ... }. Dans le catch il faut effectuer les
	 *    instructions nécessaires pour quitter proprement le traitement de la thread.
	 */
	public static void main1(String[] args) throws InterruptedException {
		Thread t = new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(1_000);
				} catch (InterruptedException e) {
					System.out.println("end");
					return;
				}
			}
		});
		t.start();
		Thread.sleep(1_000);
		t.interrupt();
	}
	
	private static int slow() {
		int result = 1;
		for (int i = 0; i < 1_000_000; i++) {
			result += (result * 7) % 513;
		}
		return result;
	}
	
    /* 
     * 3. Dans le cas d'une opération non bloquante, il est nécessaire de tester à différentes étapes de l'opération
     *    si une demande d'interruption de la thread courrante est survenue. Si tel est le cas alors il faut mettre
     *    fin à l'opération.
     *    
     *    Dans notre exemple, on va vérifier à chaque tour de boucle si la thread doit être interrompue ou non.
     */
	public static void main2(String[] args) throws InterruptedException {
	    Thread t = new Thread(() -> {
	        int forNothing = 0;
	        while (!Thread.interrupted()) { 
	        	forNothing += slow();
	        }
	        System.out.println("end " + forNothing);
	    });
	    t.start();
	    Thread.sleep(1_000);
	    t.interrupt();
	  }
	
	/*
	 * 4. Pour interrompre une thread à coup sûr, il faut gérer les InterruptedException pour les cas où la thread soit
	 *    dans une opération bloquante. La gestion de ces erreurs consiste à indiquer que la thread doit être interrompue
	 *    en utilisant la méthode d'instance interrupt(). De cette façon, la prochaine instruction qui vérifie si la thread
	 *    doit être interrompue permettra de gérer à la fois les opérations non bloquantes et les opérations bloquantes.
	 */
	public static void main3(String[] args) throws InterruptedException {
		Thread t = new Thread(() -> {
			int forNothing = slow();
			while (!Thread.interrupted()) {
				try {
					Thread.sleep(1_000);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					continue;
				}
				forNothing += slow();
			}
			System.out.println("end " + forNothing);
		});
		t.start();
		Thread.sleep(1_000);
		t.interrupt();
	}
	
	/*
	 * 5. Thread.interrupted() renvoie la valeur du flag indiquant si la thread courrante est interrompue ou non puis
	 *    positionne ce flag à false. thread.isInterrupted() est une méthode de débugage qui permet de connaître
	 *    l'état du flag sité précédemment.
	 *    
	 *    Thread.interrupted() est mal nommée car le nom de la méthode ne permet pas de savoir que la valeur du flag
	 *    peut être modifiée.
	 *    
	 * 6. Pour que le programme se termine si l'on fait Ctrl-D il faut interrompre toutes les threads qui ne sont pas
	 * démon. Si des threads non démons continuent de s'éxécuter alors la JVM ne s'arretera pas.
	 */
	private static Thread createThread(int threadNumber) {
		return new Thread(() -> {
			int counter = 0;
			while (true && !Thread.interrupted()) {
				System.out.println("(" + threadNumber + ") " + counter++);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					return;
				}
			}
		});
	}

	public static void main4(String[] args) throws InterruptedException {
		Thread threads[] = new Thread[THREAD_NUMBER];
		IntStream.range(0, THREAD_NUMBER).forEach(i -> {
			Thread t = createThread(i + 1);
			threads[i] = t;
			t.start();
		});

		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Entrez un numéro de thread pour l'arreter");
			while (scanner.hasNextInt()) {
				int threadId = scanner.nextInt();
				if (threadId >= 1 && threadId <= THREAD_NUMBER) {
					threads[threadId - 1].interrupt();
				}
			}
			for(Thread t : threads) {
				t.interrupt();
			}
		}
	}
	
	public static void main(String args[]) throws InterruptedException {
		//main1(args); // Question 2
		//main2(args); // Question 3
		//main3(args); // Question 4
		main4(args); // Question principale de l'exercice
	}
}
