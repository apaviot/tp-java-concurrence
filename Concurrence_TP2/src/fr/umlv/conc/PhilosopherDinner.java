package fr.umlv.conc;

import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class PhilosopherDinner {
	private final ReentrantLock[] forks;
	
	public PhilosopherDinner(int forkCount) {
		ReentrantLock[] forks = new ReentrantLock[forkCount];
		Arrays.setAll(forks, i -> new ReentrantLock());
		this.forks = forks;
	}
	
	public void eat(int index) {
		ReentrantLock fork1 = forks[index];
		ReentrantLock fork2 = forks[(index + 1) % forks.length];
		if (fork1.tryLock()) {
			try {
				if (fork2.tryLock()) {
					try {
						System.out.println("philosopher " + index + " eat");
					} finally {
						fork2.unlock();
					}
				}
			} finally {
				fork1.unlock();
			}
		}
	}

	public static void main(String[] args) {
		PhilosopherDinner dinner = new PhilosopherDinner(5);
		IntStream.range(0, 5).forEach(i -> {
			new Thread(() -> {
				for(;;) {
					dinner.eat(i);
				}
			}).start();
		});
	}
	
	/* ---------- Exercice 4 ----------
	 * 
	 * 1. Le problème du code ci-dessus est qu'il est succeptible de donner lieu à des dead locks.
	 *    Cela arrive si la thread 0 (T0) prend le verrou 0 (V0), que T1 prend V1, T2 prend V2, T3 prend V3,
	 *    T4 prend V4 puis que T0 essaye de prendre V1, T1 essaye de prendre V2, T2 essaye de prendre V3,
	 *    T3 essaye de prendre V4 et T4 essaye de prendre V0
	 *    
	 * 2. Il est possible d'avoir deux philosophes qui mangent en même temps. Par exemple le philosophe 0 et
	 *    le philosophe 2 peuvent manger en même temps.
	 */
}
